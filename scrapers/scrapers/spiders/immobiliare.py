import re
import sqlite3

import django
import scrapy

# if __name__ == 'scrapers.spiders.immobiliare':
#     from scrapers.items import ImmobiliareItem
# else:
#     from scrapers.scrapers.items import ImmobiliareItem
from scrapers.items import ImmobiliareItem

from main.models import House


def remove_currency(string):
    digits = re.findall(r'[\d]+', string)
    return ''.join(digits)


class Immobiliare(scrapy.Spider):
    """ immobiliare.it """
    name = 'immobiliare'

    start_urls = [
        'https://www.immobiliare.it/vendita-case/casola-valsenio/',
    ]

    def __init__(self, *args, **kwargs):
        if '_job' in kwargs:
            kwargs.pop('_job')
        super().__init__(*args, **kwargs)
        self.__item = None

    def parse(self, response):
        for item in response.css('li.listing-item.js-row-detail'):
            self.__item = item

            title = self.__get_title('p.titolo.text-primary a::text')

            price = self.__get_price('li.lif__item.lif__pricing::text',
                                     'li.lif__item.lif__pricing div::text')

            rooms = self.__get_rooms('li.lif__item:nth-child(2) div.lif__data span::text')

            description = self.__get_description('div.descrizione p::text')

            url = self.__get_url('div.listing-item_body div.listing-item_body--content p.titolo a::attr("href")')

            kwargs = {'title': title,
                      'price': price,
                      'rooms': rooms,
                      # bathrooms=None,
                      # floors=None,
                      'description':description,
                      # extension=None,
                      'url':url,
                      'scraper': self.name,
                      'active': True}

            # save a new item or update the one in the database.
            try:
                ImmobiliareItem(**kwargs).save()
            except (sqlite3.IntegrityError, django.db.utils.IntegrityError):
                House.objects.filter(url=url).update(**kwargs)

            yield {
                'title': title,
                'price': price,
                'rooms': rooms,
                'description': description,
                'url': url
            }

        next_page = response.css('ul.pull-right.pagination > li > a::attr("href")').get()
        if next_page is not None:
            yield response.follow(next_page, self.parse)

    def __get_title(self, *selectors):
        title = None
        for selector in selectors:
            title = self.__item.css(selector).get().strip()
            if title:
                break
        return title

    def __get_price(self, *selectors):
        price = ''
        for selector in selectors:
            price = self.__item.css(selector).get()
            if price:
                price = price.strip()
                break
        return int(remove_currency(price))

    def __get_rooms(self, *selectors):
        rooms = None
        for selector in selectors:
            rooms = self.__item.css(selector).get().strip()
            if rooms:
                if rooms.endswith('+'):
                    rooms = rooms[:-1]
                rooms = int(rooms)
                break
        return rooms

    def __get_description(self, *selectors):
        description = ''
        for selector in selectors:
            description = self.__item.css(selector).get()
            if description:
                description = description.strip()
                break
        return description

    def __get_url(self, *selectors):
        url = None
        for selector in selectors:
            url = self.__item.css(selector).get()
            if url:
                break
        return url
