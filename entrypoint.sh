#!/usr/bin/env bash

cd scrapers
scrapyd &

cd ..
python manage.py runserver "0.0.0.0:8000"
