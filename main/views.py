import os
from uuid import uuid4
from urllib.parse import urlparse

from django.apps import apps
from django.core.paginator import Paginator
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.shortcuts import render
from django.views.decorators.http import require_GET, require_POST, require_http_methods
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from scrapyd_api import ScrapydAPI
# from main.models import ScrapyItem

# connect scrapyd service
from main.models import House
# from scrapers.scrapers.spiders import immobiliare

scrapyd = ScrapydAPI('http://localhost:6800')


def is_valid_url(url):
    validate = URLValidator()
    try:
        validate(url)  # check if url format is valid
    except ValidationError:
        return False

    return True


def split_list_in_couples(data_list):

    def _split_list_in_couples(array, res=[]):
        if len(array) <= 2:
            res.append(array)
            return res
        else:
            res.append([array[0], array[1]])
            return split_list_in_couples(array[2:], res)

    return _split_list_in_couples(data_list)


@csrf_exempt
@require_http_methods(['POST', 'GET'])  # only get and post
def crawl(request):
    # Post requests are for new crawling tasks
    if request.method == 'POST':

        # domain = urlparse(url).netloc  # parse the url and extract the domain
        scraper = request.POST.get('scraper', None)
        unique_id = str(uuid4())  # create a unique ID.

        # This is the custom settings for scrapy spider.
        # We can send anything we want to use it inside spiders and pipelines.
        # I mean, anything
        settings = {
            'unique_id': unique_id,  # unique ID for each record for DB
            'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
        }

        scrapers_list = scrapyd.list_spiders('default')

        if scraper not in scrapers_list:
            return JsonResponse({'error': 'SCRAPER is invalid'})

        # Deactivate every item that has already been scraped by this spider.
        if scraper in ['immobiliare']:
            House.objects.filter(scraper=scraper).update(active=False)

        # Here we schedule a new crawling task from scrapyd.
        # Notice that settings is a special argument name.
        # But we can pass other arguments, though.
        # This returns a ID which belongs and will be belong to this task
        # We are goint to use that to check task's status.
        task = scrapyd.schedule('default', scraper,
                                settings=settings)  # , url=url, domain=domain)

        return JsonResponse({'task_id': task, 'unique_id': unique_id, 'status': 'started'})

    # # Get requests are for getting result of a specific crawling task
    # elif request.method == 'GET':
    #     # We were passed these from past request above. Remember ?
    #     # They were trying to survive in client side.
    #     # Now they are here again, thankfully. <3
    #     # We passed them back to here to check the status of crawling
    #     # And if crawling is completed, we respond back with a crawled data.
    #     task_id = request.GET.get('task_id', None)
    #     unique_id = request.GET.get('unique_id', None)
    #
    #     if not task_id or not unique_id:
    #         return JsonResponse({'error': 'Missing args'})
    #
    #     # Here we check status of crawling that just started a few seconds ago.
    #     # If it is finished, we can query from database and get results
    #     # If it is not finished we can return active status
    #     # Possible results are -> pending, running, finished
    #     status = scrapyd.job_status('default', task_id)
    #     if status == 'finished':
    #         try:
    #             # this is the unique_id that we created even before crawling started.
    #             item = ScrapyItem.objects.get(unique_id=unique_id)
    #             return JsonResponse({'data': item.to_dict['data']})
    #         except Exception as e:
    #             return JsonResponse({'error': str(e)})
    #     else:
    #         return JsonResponse({'status': status})


@require_GET
def scrapers(request):

    # def valid_py(filename):
    #     ext = filename[filename.rfind('.'):]
    #     return ext== '.py' and filename[0] != '_'
    #
    # def no_ext(filename):
    #     return filename[:filename.rfind('.')]
    #
    # spiders_dir_content = os.listdir('scrapers/scrapers/spiders')
    # spiders = [no_ext(x) for x in spiders_dir_content  if valid_py(x)]
    # return render(request, 'adminlte/scrapers.html', {
    #     'spiders': spiders
    # })

    # return render(request, 'adminlte/scrapers.html', {
    #     'spiders_data': [{'name': k, 'uid': v} for k, v in spiders_data.items()]
    # })

    spiders_list = scrapyd.list_spiders('default')
    spiders_status = {s: 'finished' for s in spiders_list}
    jobs = scrapyd.list_jobs('default')
    for job in jobs['pending']:
        spiders_status[job['spider']] = 'pending'
    for job in jobs['running']:
        spiders_status[job['spider']] = 'running'
    for job in jobs['finished']:
        spiders_status[job['spider']] = 'finished'

    # for scraper in data:
    #     scraper['status'] = scrapyd.job_status('default', scraper['task_id'])

    return render(request, 'adminlte/scrapers.html', {
        'spiders_data': [{'name': k, 'status': v} for k, v in spiders_status.items()]
    })


@require_GET
def database(request):
    models = apps.app_configs.get('main').models
    models_name = [m.title() for m in models]
    models_name = split_list_in_couples(models_name)
    return render(request, 'adminlte/database.html', {'models': models_name})


@require_GET
def houses(request):
    page = request.GET.get('page', 1)
    per_page = request.GET.get('per_page', 10)

    houses_data = House.objects.filter(active=True).values()
    paginator = Paginator(houses_data, per_page)
    _houses = paginator.page(page)
    per_page_values = [10, 25, 50, 100]
    return render(request, 'adminlte/houses.html', {'houses': _houses, 'per_page_values': per_page_values})
