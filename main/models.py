import json
from django.db import models
from django.utils import timezone


# class ScrapyItem(models.Model):
#     unique_id = models.CharField(max_length=100, null=True)
#     data = models.TextField()  # this stands for our crawled data
#     date = models.DateTimeField(default=timezone.now)
#
#     # This is for basic and custom serialisation to return it to client as a JSON.
#     @property
#     def to_dict(self):
#         data = {
#             'data': json.loads(self.data),
#             'date': self.date
#         }
#         return data
#
#     def __str__(self):
#         return self.unique_id


class House(models.Model):
    title = models.CharField(max_length=30)
    price = models.IntegerField()
    rooms = models.IntegerField(default=1)
    bathrooms = models.IntegerField(default=1)
    extension = models.IntegerField(default=None, blank=True, null=True)  # in m^2
    floors = models.IntegerField(default=1)
    description = models.CharField(max_length=150)
    url = models.CharField(max_length=150, unique=True)
    last_updated = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)
    scraper = models.CharField(max_length=30)
    # unique_id = models.CharField(max_length=100, null=True)
