FROM python:3

ENV PYTHONUNBUFFERED 1

RUN mkdir /scraper_backend

WORKDIR /scraper_backend

COPY . /scraper_backend/

#RUN pip install -r requirements.txt
RUN pip install pipenv && \
    pipenv install --system

EXPOSE 6800
EXPOSE 8000

CMD /scraper_backend/entrypoint.sh
