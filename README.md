### Install dependencies
```bash
pipenv install
```

### Activate pipenv
```bash
pipenv shell
```

### Start django
```bash
python manage.py runserver
```

### Launch scrapyd
```bash
cd scrapers
scrapyd
```

### Launch crawler
```bash
cd scrapers/scrapers/spiders
scrapy crawl immobiliare
```


## Alternative with Docker

### Build docker
```bash
docker build -t scraper-docker .
```

### Launch docker to test
```bash
docker run --rm -p 8000:8000 -p 6800:6800 -it scraper-docker
```
Note: this command is only for testing purpose, it destroy all data when stopped.


## Reset database
```bash
python manage.py makemigrations main
python manage.py migrate
```