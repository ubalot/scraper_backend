"""scraper_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

# urlpatterns = [
#     url(r'^admin/', admin.site.urls),
# ]

# from django.conf import settings
# from django.conf.urls import url, static
# from django.contrib import admin
from django.views.generic import TemplateView
from main import views
# from main.views import TemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='adminlte/index.html'), name='home'),
    url(r'^database/$', views.database, name='database'),
    url(r'^database/houses', views.houses, name='houses'),
    # url(r'^scrapers/', TemplateView.as_view(template_name='adminlte/scrapers.html'), name='scrapers'),
    url(r'^scrapers/$', views.scrapers, name='scrapers'),
    url(r'^api/crawl/', views.crawl, name='crawl'),
]

# # This is required for static files while in development mode. (DEBUG=TRUE)
# # No, not relevant to scrapy or crawling :)
# if settings.DEBUG:
#     urlpatterns += static.static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
